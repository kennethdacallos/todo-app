/**
 * File: src/features/duties/components/DutiesTable.test.tsx
 * Description: This file contains tests for the DutiesTable component.
 */

import React from 'react';
import { render, screen, within, fireEvent } from '@testing-library/react';
import DutiesTable from '../features/duties/components/DutiesTable';
import { Duty } from '../features/duties/types/duty';

const mockDuties: Duty[] = [{ id: 1, name: 'Wash the car' }];

const mockToggleSelectDuty = jest.fn();
const mockStartEditingDuty = jest.fn();
const mockDeleteDuty = jest.fn();

window.matchMedia =
  window.matchMedia ||
  function () {
    return {
      matches: false,
      addListener: function () {},
      removeListener: function () {},
    };
  };

describe('DutiesTable', () => {
  it('renders duties correctly', () => {
    render(
      <DutiesTable
        duties={mockDuties}
        selectedDuties={[]}
        toggleSelectDuty={mockToggleSelectDuty}
        startEditingDuty={mockStartEditingDuty}
        deleteDuty={mockDeleteDuty}
      />
    );

    expect(screen.getByText(/Wash the car/i)).toBeInTheDocument();
  });

  it('calls toggleSelectDuty when a checkbox is clicked', () => {
    render(
      <DutiesTable
        duties={mockDuties}
        selectedDuties={[]}
        toggleSelectDuty={mockToggleSelectDuty}
        startEditingDuty={mockStartEditingDuty}
        deleteDuty={mockDeleteDuty}
      />
    );

    fireEvent.click(screen.getAllByRole('checkbox')[0]);
    expect(mockToggleSelectDuty).toHaveBeenCalledWith(1);
  });

  it('calls startEditingDuty when the edit button is clicked', () => {
    render(
      <DutiesTable
        duties={mockDuties}
        selectedDuties={[]}
        toggleSelectDuty={mockToggleSelectDuty}
        startEditingDuty={mockStartEditingDuty}
        deleteDuty={mockDeleteDuty}
      />
    );

    fireEvent.click(screen.getByText('Edit'));
    expect(mockStartEditingDuty).toHaveBeenCalledWith(mockDuties[0]);
  });

  it('calls deleteDuty when the delete button is clicked', () => {
    render(
      <DutiesTable
        duties={mockDuties}
        selectedDuties={[]}
        toggleSelectDuty={mockToggleSelectDuty}
        startEditingDuty={mockStartEditingDuty}
        deleteDuty={mockDeleteDuty}
      />
    );

    fireEvent.click(screen.getByText('Delete'));
    expect(mockDeleteDuty).toHaveBeenCalledWith(1);
  });
});
