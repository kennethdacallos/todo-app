/**
 * File: src/features/duties/api/dutiesApi.ts
 * Description: This file contains API calls for the duties feature.
 */

import axios from 'axios';
import { Duty } from '../types/duty';

// Base URL for the duties API
const API_URL = 'http://localhost:3000/api/duties';

/**
 * Fetches all duties from the server.
 * @returns {Promise<Duty[]>} - A promise that resolves to an array of duties.
 */
export const getDuties = async (): Promise<Duty[]> => {
  const response = await axios.get(API_URL);
  return response.data.data.duties;
};

/**
 * Adds a new duty to the server.
 * @param {string} name - The name of the new duty.
 * @returns {Promise<Duty>} - A promise that resolves to the added duty.
 */
export const addDuty = async (name: string): Promise<Duty> => {
  const response = await axios.post(API_URL, { name });
  return response.data.data.duty;
};

/**
 * Updates an existing duty on the server.
 * @param {number} id - The ID of the duty to update.
 * @param {string} name - The new name of the duty.
 * @returns {Promise<Duty>} - A promise that resolves to the updated duty.
 */
export const updateDuty = async (id: number, name: string): Promise<Duty> => {
  const response = await axios.put(`${API_URL}/${id}`, { name });
  return response.data.data.duty;
};

/**
 * Deletes a duty from the server.
 * @param {number} id - The ID of the duty to delete.
 * @returns {Promise<void>} - A promise that resolves when the duty is deleted.
 */
export const deleteDuty = async (id: number): Promise<void> => {
  await axios.delete(`${API_URL}/${id}`);
};

/**
 * Deletes multiple duties from the server.
 * @param {number[]} ids - An array of duty IDs to delete.
 * @returns {Promise<void>} - A promise that resolves when the duties are deleted.
 */
export const deleteMultipleDuties = async (ids: number[]): Promise<void> => {
  await axios.delete(API_URL, { data: { ids } });
};
