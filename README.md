# Todo Web Application

This is a full-stack web application for managing duties using Node.js, Express, TypeScript, React, PostgreSQL, and Docker. The application allows users to create, read, update, and delete duties.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Running the Application](#running-the-application)
- [Running Tests](#running-tests)
- [Folder Structure](#folder-structure)

## Prerequisites

Make sure you have the following software installed on your machine:

- [Node.js](https://nodejs.org/en/download/)
- [Docker](https://www.docker.com/products/docker-desktop)
- [Git](https://git-scm.com/)

## Installation

### Clone the Repository

```sh
git clone https://gitlab.com/kennethdacallos/todo-app
cd todo-app
```

### Environment Variables

Create a .env file in the server directory and add the following environment variables:

```env
# Database
POSTGRES_USER=myuser
POSTGRES_HOST=db
POSTGRES_DB=mydatabase
POSTGRES_PASSWORD=mypassword
POSTGRES_PORT=5432

# Server
PORT=3000
```

## Install Dependencies

### Backend

```sh
cd server
npm install
```

## Frontend

```sh
cd ../client
npm install
```

## Running the Application

### Using Docker

This setup uses Docker to simplify the process of running the application.

##### 1. Build and Run the Docker Containers

From the root directory of the project:

```sh
docker-compose up --build
```

##### 2. Access the Application

Backend API: http://localhost:3000/api/duties
Frontend: http://localhost:3001

### Without Docker

### Running the Backend

##### 1. Create the PostgreSQL Database

Ensure PostgreSQL is running and create a database:

```sh
createdb duties_db
```

##### 2. Run the Backend Server

```sh
cd server
npm run build
npm start
```

##### 3. Access the Backend Server

http://localhost:3000/api/duties

### Running the Frontend

##### 1. Run the Frontend Development Server

```sh
cd client
npm start
```

##### 2. Access the Frontend

Open http://localhost:3001 in your browser.

## Running Tests

### Backend Tests

#### 1. Navigate to the Server Directory

```sh
cd server
```

#### 2. Run the Tests

```sh
npm test
```

### Frontend Tests

#### 1. Navigate to the Client Directory

```sh
cd client
```

#### 2. Run the Tests

```sh
npm test
```

## Folder Structure

```bash
todo-app/
├── client/                # React frontend application
│   ├── public/
│   ├── src/
│   ├── Dockerfile
│   ├── package.json
│   └── ...
├── server/                # Node.js backend application
│   ├── src/
│   ├── Dockerfile
│   ├── package.json
│   └── ...
├── db_init/               # Docker database initiliazation
├── docker-compose.yml     # Docker compose configuration
└── README.md              # Project documentation
```
