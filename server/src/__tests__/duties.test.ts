/**
 * File: src/__tests__/duties.test.ts
 * Description: Jest test file for testing the Duties API endpoints.
 */

import request, { Response } from 'supertest';
import express, { Express } from 'express';
import { QueryResult } from 'pg';
import dutiesRouter from '../routes/duties';
import db from '../utils/db';
import { Duty } from '../models/duties';

// Mock the database
jest.mock('../utils/db');

const app: Express = express();
app.use(express.json());
app.use('/api', dutiesRouter);

describe('Duties API', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  /**
   * Test to return all duties
   * This test verifies that all duties can be retrieved successfully.
   */
  it('should return all duties', async () => {
    const duties: Duty[] = [{ id: 1, name: 'Test Duty' }];
    (db.query as jest.Mock).mockResolvedValueOnce({
      rows: duties,
    } as QueryResult<Duty>);

    const response: Response = await request(app).get('/api/duties');

    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      status: 'success',
      data: { duties },
    });
  });

  /**
   * Test to create a new duty
   * This test verifies that a single duty can be created successfully.
   */
  it('should create a new duty', async () => {
    const newDuty: Duty = { id: 2, name: 'New Duty' };
    (db.query as jest.Mock).mockResolvedValueOnce({
      rows: [newDuty],
    } as QueryResult<Duty>);

    const response: Response = await request(app)
      .post('/api/duties')
      .send({ name: 'New Duty' });

    expect(response.status).toBe(201);
    expect(response.body).toEqual({
      status: 'success',
      data: { duty: newDuty },
    });
  });

  /**
   * Test to return a single duty
   * This test verifies that a single duty can be retrieved successfully by ID.
   */
  it('should return a single duty', async () => {
    const duty: Duty = { id: 1, name: 'Test Duty' };
    (db.query as jest.Mock).mockResolvedValueOnce({
      rows: [duty],
    } as QueryResult<Duty>);

    const response: Response = await request(app).get('/api/duties/1');

    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      status: 'success',
      data: { duty },
    });
  });

  /**
   * Test to update a duty
   * This test verifies that a duty can be updated successfully.
   */
  it('should update a duty', async () => {
    const updatedDuty: Duty = { id: 1, name: 'Updated Duty' };
    (db.query as jest.Mock).mockResolvedValueOnce({
      rows: [updatedDuty],
    } as QueryResult<Duty>);

    const response: Response = await request(app)
      .put('/api/duties/1')
      .send({ name: 'Updated Duty' });

    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      status: 'success',
      data: { duty: updatedDuty },
    });
  });

  /**
   * Test to delete a duty
   * This test verifies that a single duty can be deleted successfully by ID.
   */
  it('should delete a duty', async () => {
    const deletedDuty: Duty = { id: 1, name: 'Test Duty' };
    (db.query as jest.Mock).mockResolvedValueOnce({
      rows: [deletedDuty],
    } as QueryResult<Duty>);

    const response: Response = await request(app).delete('/api/duties/1');

    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      status: 'success',
      data: { duty: deletedDuty },
    });
  });

  /**
   * Test to delete multiple duties
   * This test verifies that multiple duties can be deleted successfully by sending an array of IDs.
   */
  it('should delete multiple duties', async () => {
    const duties: Duty[] = [
      { id: 1, name: 'Test Duty 1' },
      { id: 2, name: 'Test Duty 2' },
    ];
    (db.query as jest.Mock).mockResolvedValueOnce({
      rows: duties,
    } as QueryResult<Duty>);

    const response: Response = await request(app)
      .delete('/api/duties')
      .send({ ids: [1, 2] });

    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      status: 'success',
      data: { duties },
    });
  });
});
