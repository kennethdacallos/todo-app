/**
 * File: src/helpers/response.ts
 * Description: This file contains helper functions for standardized API responses.
 */

/**
 * Generates a standardized success response.
 * @param {any} data - The data to include in the response.
 * @returns {object} - The standardized success response object.
 */
export const success = (data: any) => {
  return {
    status: 'success',
    data: Array.isArray(data) ? { duties: data } : { duty: data },
  };
};

/**
 * Generates a standardized validation error response.
 * @param {Array<object>} errors - An array of validation error objects.
 * @returns {object} - The standardized validation error response object.
 */
export const validation = (errors: { field: string; msg: string }[]) => {
  const formattedErrors: { [key: string]: string } = {};
  errors.forEach((error) => {
    formattedErrors[error.field] = error.msg;
  });

  return {
    status: 'fail',
    data: formattedErrors,
  };
};

/**
 * Generates a standardized error response.
 * @param {string} message - The error message.
 * @returns {object} - The standardized error response object.
 */
export const error = (message: string) => {
  return {
    status: 'error',
    message,
  };
};
