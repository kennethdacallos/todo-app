/**
 * File: src/controllers/dutyController.ts
 * Description: This file contains request handlers for duties.
 */

import { Request, Response } from 'express';
import * as dutyService from '../services/dutyService';
import logger from '../config/logger';
import { Duty } from '../models/duties';

// Handler to get all duties
export const getAllDuties = async (req: Request, res: Response) => {
  try {
    const duties = await dutyService.getAllDuties();
    res.status(200).json({ status: 'success', data: { duties } });
  } catch (err) {
    logger.error('Error retrieving duties', err);
    res
      .status(500)
      .json({ status: 'error', message: 'Unable to retrieve data' });
  }
};

// Handler to create a new duty
export const createDuty = async (req: Request, res: Response) => {
  const { name } = req.body as Duty;
  if (!name) {
    return res
      .status(422)
      .json({ status: 'fail', data: { name: 'Name is required' } });
  }
  try {
    const duty = await dutyService.createDuty(name);
    res.status(201).json({ status: 'success', data: { duty } });
  } catch (err) {
    logger.error('Error creating duty', err);
    res.status(500).json({ status: 'error', message: 'Unable to insert data' });
  }
};

// Handler to get a single duty by ID
export const getDutyById = async (req: Request, res: Response) => {
  const id = parseInt(req.params.id);
  try {
    const duty = await dutyService.getDutyById(id);
    if (!duty) {
      return res
        .status(404)
        .json({ status: 'error', message: 'No data found' });
    }
    res.status(200).json({ status: 'success', data: { duty } });
  } catch (err) {
    logger.error('Error retrieving duty', err);
    res
      .status(500)
      .json({ status: 'error', message: 'Unable to retrieve data' });
  }
};

// Handler to update a duty
export const updateDuty = async (req: Request, res: Response) => {
  const id = parseInt(req.params.id);
  const { name } = req.body as Duty;
  if (!name) {
    return res
      .status(400)
      .json({ status: 'fail', data: { name: 'Name is required' } });
  }
  try {
    const duty = await dutyService.updateDuty(id, name);
    if (!duty) {
      return res
        .status(404)
        .json({ status: 'error', message: 'No data found' });
    }
    res.status(200).json({ status: 'success', data: { duty } });
  } catch (err) {
    logger.error('Error updating duty', err);
    res.status(500).json({ status: 'error', message: 'Unable to update data' });
  }
};

// Handler to delete a duty
export const deleteDuty = async (req: Request, res: Response) => {
  const id = parseInt(req.params.id);
  try {
    const duty = await dutyService.deleteDuty(id);
    if (!duty) {
      return res
        .status(404)
        .json({ status: 'error', message: 'No data found' });
    }
    res.status(200).json({ status: 'success', data: { duty } });
  } catch (err) {
    logger.error('Error deleting duty', err);
    res.status(500).json({ status: 'error', message: 'Error deleting duty' });
  }
};

// Handler to delete multiple duties
export const deleteMultipleDuties = async (req: Request, res: Response) => {
  const { ids } = req.body;
  if (!Array.isArray(ids) || ids.length === 0) {
    return res
      .status(400)
      .json({ status: 'fail', data: { ids: 'IDs are required' } });
  }
  try {
    const duties = await dutyService.deleteMultipleDuties(ids);
    res.status(200).json({ status: 'success', data: { duties } });
  } catch (err) {
    logger.error('Error deleting duties', err);
    res.status(500).json({ status: 'error', message: 'Error deleting duties' });
  }
};
