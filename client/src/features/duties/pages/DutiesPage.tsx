/**
 * File: src/features/duties/pages/DutiesPage.tsx
 * Description: This file contains the main component for managing duties.
 */

import React, { useState } from 'react';
import { Button, Form, Input, Modal, message } from 'antd';
import DutiesTable from '../components/DutiesTable';
import { useDuties } from '../context/DutiesContext';
import { Duty } from '../types/duty';

const DutiesPage: React.FC = () => {
  // Destructure context state and actions
  const {
    duties,
    loading,
    error,
    addNewDuty,
    editDuty,
    removeDuty,
    removeMultipleDuties,
  } = useDuties();

  // State for new duty input, selected duties, and editing duty
  const [newDuty, setNewDuty] = useState<string>('');
  const [selectedDuties, setSelectedDuties] = useState<number[]>([]);
  const [editingDuty, setEditingDuty] = useState<Duty | null>(null);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [form] = Form.useForm();

  /**
   * Toggles the selection of a duty.
   * @param {number} id - The ID of the duty to toggle selection.
   */
  const toggleSelectDuty = (id: number) => {
    setSelectedDuties((prev) =>
      prev.includes(id) ? prev.filter((dutyId) => dutyId !== id) : [...prev, id]
    );
  };

  /**
   * Starts editing a duty by opening the modal and setting the form fields.
   * @param {Duty} duty - The duty to edit.
   */
  const startEditingDuty = (duty: Duty) => {
    setEditingDuty(duty);
    setIsModalVisible(true);
    form.setFieldsValue({ name: duty.name });
  };

  /**
   * Handles adding a new duty by calling the addNewDuty function from the context.
   */
  const handleAddDuty = async () => {
    if (newDuty.length < 3) {
      message.error('Duty name must be at least 3 characters long');
      return;
    }
    try {
      await addNewDuty(newDuty);
      setNewDuty('');
      form.resetFields();
    } catch (error) {
      message.error('Failed to add duty');
    }
  };

  /**
   * Handles updating a duty by calling the editDuty function from the context.
   */
  const handleUpdateDuty = async () => {
    if (editingDuty) {
      try {
        await editDuty(editingDuty.id, editingDuty.name);
        setEditingDuty(null);
        setIsModalVisible(false);
      } catch (error) {
        message.error('Failed to update duty');
      }
    }
  };

  /**
   * Handles deleting selected duties by calling the removeMultipleDuties function from the context.
   */
  const handleDeleteSelectedDuties = async () => {
    try {
      await removeMultipleDuties(selectedDuties);
      setSelectedDuties([]);
    } catch (error) {
      message.error('Failed to delete selected duties');
    }
  };

  return (
    <div style={{ padding: '20px' }}>
      <h1>Todo</h1>
      <Form layout='inline' onFinish={handleAddDuty}>
        <Form.Item
          name='newDuty'
          rules={[
            { required: true, message: 'Please enter the duty name' },
            { min: 3, message: 'Duty name must be at least 3 characters long' },
          ]}
        >
          <Input
            value={newDuty}
            onChange={(e) => setNewDuty(e.target.value)}
            placeholder='New duty'
          />
        </Form.Item>
        <Form.Item>
          <Button type='primary' htmlType='submit'>
            Add Duty
          </Button>
        </Form.Item>
        <Form.Item>
          <Button
            danger
            onClick={handleDeleteSelectedDuties}
            disabled={selectedDuties.length === 0}
          >
            Delete Selected Duties
          </Button>
        </Form.Item>
      </Form>
      <DutiesTable
        duties={duties}
        selectedDuties={selectedDuties}
        toggleSelectDuty={toggleSelectDuty}
        startEditingDuty={startEditingDuty}
        deleteDuty={removeDuty}
      />
      <Modal
        title='Edit Duty'
        open={isModalVisible}
        onOk={handleUpdateDuty}
        onCancel={() => {
          setIsModalVisible(false);
          form.resetFields();
        }}
      >
        <Form form={form} layout='vertical'>
          <Form.Item
            name='name'
            rules={[
              { required: true, message: 'Please enter the duty name' },
              {
                min: 3,
                message: 'Duty name must be at least 3 characters long',
              },
            ]}
          >
            <Input
              value={editingDuty?.name}
              onChange={(e) =>
                setEditingDuty({ ...editingDuty, name: e.target.value } as Duty)
              }
              placeholder='Edit duty'
            />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default DutiesPage;
