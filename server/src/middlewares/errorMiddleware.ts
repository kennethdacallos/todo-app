/**
 * File: src/middlewares/errorMiddleware.ts
 * Description: This file contains the error handling middleware.
 */

import { Request, Response, NextFunction } from 'express';
import logger from '../config/logger';

// Middleware function for handling errors
export const errorHandler = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  logger.error('Unhandled error', err);
  res.status(500).send({ status: 'error', message: 'Something went wrong!' });
};
