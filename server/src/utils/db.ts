/**
 * File: src/utils/db.ts
 * Description: This file sets up the database connection pool.
 */

import { Pool } from 'pg';
import { config } from '../config/config';
import logger from '../config/logger';

// Create a new pool of connections for PostgreSQL
const pool = new Pool(config.db);

pool.on('connect', () => {
  logger.info('Connected to the database');
});

pool.on('error', (err) => {
  logger.error('Database error', err);
});

// Export a query function to be used in other parts of the application
export default {
  query: (text: string, params?: any[]) => {
    logger.debug('Executing query', { text, params });
    return pool.query(text, params);
  },
};
