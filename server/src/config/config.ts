/**
 * File: src/config/config.ts
 * Description: This file contains configuration settings for the application.
 */

import 'dotenv/config';

// Export the configuration settings
export const config = {
  // Server port
  port: process.env.PORT || 3000,
  // Database configuration
  db: {
    user: process.env.POSTGRES_USER || 'myuser', // Database username
    host: process.env.POSTGRES_HOST || 'db', // Database host
    database: process.env.POSTGRES_DB || 'mydatabase', // Database name
    password: process.env.POSTGRES_PASSWORD || 'mypassword', // Database password
    port: parseInt(process.env.POSTGRES_PORT || '5432', 10), // Database port
  },
  // Application environment (development, production, etc.)
  environment: process.env.NODE_ENV || 'development',
};
