/**
 * File: src/config/logger.ts
 * Description: This file sets up the logging configuration.
 */

import { createLogger, format, transports } from 'winston';
import { config } from './config';

// Create a new logger instance
const logger = createLogger({
  // Set logging level based on environment
  level: config.environment === 'production' ? 'info' : 'debug',
  format: format.combine(
    format.timestamp(), // Add timestamp to logs
    format.errors({ stack: true }), // Include error stack in logs
    format.splat(), // String interpolation for logs
    format.json() // Log in JSON format
  ),
  defaultMeta: { service: 'user-service' }, // Default metadata for logs
  transports: [
    // Log to console with colorized output in development
    new transports.Console({
      format: format.combine(format.colorize(), format.simple()),
    }),
    // Log errors to error.log file
    new transports.File({ filename: 'error.log', level: 'error' }),
    // Log all messages to combined.log file
    new transports.File({ filename: 'combined.log' }),
  ],
});

export default logger;
