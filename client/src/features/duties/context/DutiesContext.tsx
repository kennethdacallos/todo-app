/**
 * File: src/features/duties/context/DutiesContext.tsx
 * Description: This file contains the context and provider for managing duties state.
 */

import React, {
  createContext,
  useState,
  useEffect,
  useContext,
  ReactNode,
} from 'react';
import { Duty } from '../types/duty';
import {
  getDuties,
  addDuty,
  updateDuty,
  deleteDuty,
  deleteMultipleDuties,
} from '../api/dutiesApi';
import { message } from 'antd';

// Define the shape of the context's state and actions
interface DutiesContextProps {
  duties: Duty[];
  loading: boolean;
  error: string | null;
  fetchDuties: () => void;
  addNewDuty: (name: string) => Promise<void>;
  editDuty: (id: number, name: string) => Promise<void>;
  removeDuty: (id: number) => Promise<void>;
  removeMultipleDuties: (ids: number[]) => Promise<void>;
}

// Create the context with a default undefined value
const DutiesContext = createContext<DutiesContextProps | undefined>(undefined);

/**
 * DutiesProvider component that wraps its children with DutiesContext.Provider
 * to provide duties state and actions.
 */
export const DutiesProvider: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  const [duties, setDuties] = useState<Duty[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);

  /**
   * Fetches duties from the server and updates the state.
   */
  const fetchDuties = async () => {
    setLoading(true);
    try {
      const data = await getDuties();
      setDuties(data);
    } catch (error: any) {
      setError(error.message);
      message.error('Failed to fetch duties');
    } finally {
      setLoading(false);
    }
  };

  /**
   * Adds a new duty and updates the state.
   * @param {string} name - The name of the new duty.
   */
  const addNewDuty = async (name: string) => {
    try {
      const newDuty = await addDuty(name);
      setDuties([...duties, newDuty]);
      message.success('Duty added successfully');
    } catch (error: any) {
      setError(error.message);
      message.error('Failed to add duty');
    }
  };

  /**
   * Edits an existing duty and updates the state.
   * @param {number} id - The ID of the duty to edit.
   * @param {string} name - The new name of the duty.
   */
  const editDuty = async (id: number, name: string) => {
    try {
      const updatedDuty = await updateDuty(id, name);
      setDuties(duties.map((duty) => (duty.id === id ? updatedDuty : duty)));
      message.success('Duty updated successfully');
    } catch (err) {
      // setError(err.message);
      message.error('Failed to update duty');
    }
  };

  /**
   * Removes a duty and updates the state.
   * @param {number} id - The ID of the duty to remove.
   */
  const removeDuty = async (id: number) => {
    try {
      await deleteDuty(id);
      setDuties(duties.filter((duty) => duty.id !== id));
      message.success('Duty deleted successfully');
    } catch (err) {
      // setError(err.message);
      message.error('Failed to delete duty');
    }
  };

  /**
   * Removes multiple duties and updates the state.
   * @param {number[]} ids - The IDs of the duties to remove.
   */
  const removeMultipleDuties = async (ids: number[]) => {
    try {
      await deleteMultipleDuties(ids);
      setDuties(duties.filter((duty) => !ids.includes(duty.id)));
      message.success('Duties deleted successfully');
    } catch (err) {
      // setError(err.message);
      message.error('Failed to delete duties');
    }
  };

  // Fetch duties when the component mounts
  useEffect(() => {
    fetchDuties();
  }, []);

  // Provide state and actions to the context consumers
  return (
    <DutiesContext.Provider
      value={{
        duties,
        loading,
        error,
        fetchDuties,
        addNewDuty,
        editDuty,
        removeDuty,
        removeMultipleDuties,
      }}
    >
      {children}
    </DutiesContext.Provider>
  );
};

/**
 * Custom hook to use the DutiesContext.
 * @returns {DutiesContextProps} - The duties context value.
 */
export const useDuties = (): DutiesContextProps => {
  const context = useContext(DutiesContext);
  if (!context) {
    throw new Error('useDuties must be used within a DutiesProvider');
  }
  return context;
};
