-- File: db_init/init.sql
-- Description: Initialization script to create the database schema and table.

CREATE TABLE IF NOT EXISTS duties (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);
