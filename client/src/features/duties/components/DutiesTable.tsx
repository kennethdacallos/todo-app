/**
 * File: src/features/duties/components/DutiesTable.tsx
 * Description: This file contains the table component for displaying duties.
 */

import React from 'react';
import { Table, Button, Checkbox } from 'antd';
import { Duty } from '../types/duty'; // Updated import statement
import type { ColumnsType } from 'antd/es/table';

// Define the props for the DutiesTable component
interface DutiesTableProps {
  duties: Duty[];
  selectedDuties: number[];
  toggleSelectDuty: (id: number) => void;
  startEditingDuty: (duty: Duty) => void;
  deleteDuty: (id: number) => void;
}

/**
 * DutiesTable component
 * @param {DutiesTableProps} props - The props for the DutiesTable component
 * @returns {JSX.Element} The rendered DutiesTable component
 */
const DutiesTable: React.FC<DutiesTableProps> = ({
  duties,
  selectedDuties,
  toggleSelectDuty,
  startEditingDuty,
  deleteDuty,
}) => {
  // Define table columns
  const columns: ColumnsType<Duty> = [
    {
      title: 'Select',
      dataIndex: 'select',
      render: (_: any, record: Duty) => (
        <Checkbox
          checked={selectedDuties.includes(record.id)}
          onChange={() => toggleSelectDuty(record.id)}
        />
      ),
    },
    {
      title: 'Duty Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      render: (_: any, record: Duty) => (
        <>
          <Button type='link' onClick={() => startEditingDuty(record)}>
            Edit
          </Button>
          <Button type='link' danger onClick={() => deleteDuty(record.id)}>
            Delete
          </Button>
        </>
      ),
    },
  ];

  return (
    <Table
      rowKey='id'
      columns={columns}
      dataSource={duties}
      pagination={false}
    />
  );
};

export default DutiesTable;
