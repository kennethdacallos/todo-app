/**
 * File: src/App.tsx
 * Description: Main application component.
 */

import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { DutiesProvider } from './features/duties/context/DutiesContext';
import DutiesPage from './features/duties/pages/DutiesPage';

const App: React.FC = () => {
  return (
    <DutiesProvider>
      <Router>
        <Routes>
          <Route path='/' Component={DutiesPage} />
        </Routes>
      </Router>
    </DutiesProvider>
  );
};

export default App;
