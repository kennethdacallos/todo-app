/**
 * File: src/features/duties/types/duty.ts
 * Description: This file contains the TypeScript type definition for the Duty entity.
 */

/**
 * Duty type definition.
 * Represents a duty with an id and a name.
 */
export interface Duty {
  id: number; // Unique identifier for the duty
  name: string; // Name of the duty
}
