/**
 * File: src/repositories/dutyRepository.ts
 * Description: This file contains the data access layer for duties.
 */

import db from '../utils/db';
import { Duty } from '../models/duties';

// Function to get all duties
export const getAllDuties = async (): Promise<Duty[]> => {
  const result = await db.query('SELECT * FROM duties');
  return result.rows;
};

// Function to create a new duty
export const createDuty = async (name: string): Promise<Duty> => {
  const result = await db.query(
    'INSERT INTO duties (name) VALUES ($1) RETURNING *',
    [name]
  );
  return result.rows[0];
};

// Function to get a duty by ID
export const getDutyById = async (id: number): Promise<Duty | null> => {
  const result = await db.query('SELECT * FROM duties WHERE id = $1', [id]);
  if (result.rows.length === 0) return null;
  return result.rows[0];
};

// Function to update a duty
export const updateDuty = async (
  id: number,
  name: string
): Promise<Duty | null> => {
  const result = await db.query(
    'UPDATE duties SET name = $1 WHERE id = $2 RETURNING *',
    [name, id]
  );
  if (result.rows.length === 0) return null;
  return result.rows[0];
};

// Function to delete a duty
export const deleteDuty = async (id: number): Promise<Duty | null> => {
  const result = await db.query(
    'DELETE FROM duties WHERE id = $1 RETURNING *',
    [id]
  );
  if (result.rows.length === 0) return null;
  return result.rows[0];
};

// Function to delete multiple duties
export const deleteMultipleDuties = async (ids: number[]): Promise<Duty[]> => {
  const result = await db.query(
    'DELETE FROM duties WHERE id = ANY($1::int[]) RETURNING *',
    [ids]
  );
  return result.rows;
};
