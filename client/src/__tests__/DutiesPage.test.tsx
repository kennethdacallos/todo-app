/**
 * File: src/features/duties/pages/DutiesPage.test.tsx
 * Description: This file contains tests for the DutiesPage component.
 */

import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { DutiesProvider } from '../features/duties/context/DutiesContext';
import DutiesPage from '../features/duties/pages/DutiesPage';
import { Duty } from '../features/duties/types/duty';
import fetchMock from 'jest-fetch-mock';

fetchMock.enableMocks();

const mockDuties: Duty[] = [{ id: 1, name: 'Wash the Dishes' }];

window.matchMedia =
  window.matchMedia ||
  function () {
    return {
      matches: false,
      addListener: function () {},
      removeListener: function () {},
    };
  };

describe('DutiesPage', () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  it('adds a new duty', async () => {
    fetchMock.mockResponses(
      [JSON.stringify({ data: { duties: mockDuties } }), { status: 200 }],
      [
        JSON.stringify({ data: { duty: { id: 2, name: 'New Duty' } } }),
        { status: 200 },
      ]
    );

    render(
      <DutiesProvider>
        <DutiesPage />
      </DutiesProvider>
    );

    const textbox = screen.getByPlaceholderText('New duty');
    expect(textbox).toBeInTheDocument();
    expect(textbox).toHaveAttribute('id', 'newDuty');
    expect(textbox).toHaveAttribute('type', 'text');
    expect(textbox).toHaveValue('');

    fireEvent.change(textbox, {
      target: { value: 'New Duty' },
    });

    fireEvent.click(screen.getByText('Add Duty'));

    expect(await screen.findByText('New Duty')).toBeInTheDocument();
  });

  it('updates a duty', async () => {
    fetchMock.mockResponses(
      [JSON.stringify({ data: { duties: mockDuties } }), { status: 200 }],
      [
        JSON.stringify({ data: { duty: { id: 1, name: 'Updated Duty' } } }),
        { status: 200 },
      ]
    );

    render(
      <DutiesProvider>
        <DutiesPage />
      </DutiesProvider>
    );

    fireEvent.click(await screen.findByText('Edit'));
    fireEvent.change(screen.getByPlaceholderText('Edit duty'), {
      target: { value: 'Updated Duty' },
    });
    fireEvent.click(screen.getByText('OK'));

    expect(await screen.findByText('Updated Duty')).toBeInTheDocument();
  });

  it('deletes a duty', async () => {
    fetchMock.mockResponses(
      [JSON.stringify({ data: { duties: mockDuties } }), { status: 200 }],
      ['', { status: 200 }]
    );

    render(
      <DutiesProvider>
        <DutiesPage />
      </DutiesProvider>
    );

    fireEvent.click(await screen.findByText('Delete'));

    await waitFor(() =>
      expect(screen.queryByText('Test Duty')).not.toBeInTheDocument()
    );
  });

  it('deletes selected duties', async () => {
    fetchMock.mockResponses(
      [JSON.stringify({ data: { duties: mockDuties } }), { status: 200 }],
      ['', { status: 200 }]
    );

    render(
      <DutiesProvider>
        <DutiesPage />
      </DutiesProvider>
    );

    fireEvent.click(await screen.findByRole('checkbox'));
    fireEvent.click(screen.getByText('Delete Selected Duties'));

    await waitFor(() =>
      expect(screen.queryByText('Test Duty')).not.toBeInTheDocument()
    );
  });
});
