/**
 * File: jest.config.js
 * Description: Jest configuration file for setting up testing environment and options.
 */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: ['**/__tests__/**/*.ts', '**/?(*.)+(spec|test).ts'],
};
