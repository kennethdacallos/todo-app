/**
 * File: src/routes/duties.ts
 * Description: This file contains the routes for managing Duty data.
 */

import { Router } from 'express';
import * as dutyController from '../controllers/dutyController';

const router = Router();

router.get('/duties', dutyController.getAllDuties);
router.post('/duties', dutyController.createDuty);
router.get('/duties/:id', dutyController.getDutyById);
router.put('/duties/:id', dutyController.updateDuty);
router.delete('/duties/:id', dutyController.deleteDuty);
router.delete('/duties', dutyController.deleteMultipleDuties);

export default router;
