/**
 * File: src/index.ts
 * Description: This is the entry point for the application.
 */

import express from 'express';
import cors from 'cors';
import { config } from './config/config';
import dutyRoutes from './routes/duties';
import { errorHandler } from './middlewares/errorMiddleware';
import logger from './config/logger';

const app = express();

// Configure CORS
app.use(
  cors({
    origin: ['http://localhost:3001', 'http://localhost'],
  })
);

app.use(express.json());
app.use('/api', dutyRoutes);
app.use(errorHandler);

const port = config.port;
app.listen(port, () => {
  logger.info(`Server running on port ${port}`);
});
