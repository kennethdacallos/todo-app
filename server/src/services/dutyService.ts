/**
 * File: src/services/dutyService.ts
 * Description: This file contains business logic for duties.
 */

import { Duty } from '../models/duties';
import * as dutyRepository from '../repositories/dutyRepository';

// Function to get all duties
export const getAllDuties = async (): Promise<Duty[]> => {
  return await dutyRepository.getAllDuties();
};

// Function to create a new duty
export const createDuty = async (name: string): Promise<Duty> => {
  return await dutyRepository.createDuty(name);
};

// Function to get a duty by ID
export const getDutyById = async (id: number): Promise<Duty | null> => {
  return await dutyRepository.getDutyById(id);
};

// Function to update a duty
export const updateDuty = async (
  id: number,
  name: string
): Promise<Duty | null> => {
  return await dutyRepository.updateDuty(id, name);
};

// Function to delete a duty
export const deleteDuty = async (id: number): Promise<Duty | null> => {
  return await dutyRepository.deleteDuty(id);
};

// Function to delete multiple duties
export const deleteMultipleDuties = async (ids: number[]): Promise<Duty[]> => {
  return await dutyRepository.deleteMultipleDuties(ids);
};
